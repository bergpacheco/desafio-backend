import {
  Controller,
  Post,
  Body,
  Res,
  Get,
  Delete,
  Param,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { ItemService } from './item.service';
import { Item } from '../database/models/item.entity';
import { Response } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('item')
export class ItemController {
  constructor(private itemService: ItemService) {}

  @Post()
  async create(@Body() dto: Item, @Res() res: Response): Promise<any> {
    return res.json(await this.itemService.novoItem(dto));
  }

  @Get()
  async listItem(@Res() res: Response): Promise<any> {
    return res.json(await this.itemService.listItem());
  }

  @Delete('/:id')
  async deleteItem(@Param('id') id: number): Promise<any> {
    return await this.itemService.deleteItem(id);
  }

  @Post('parse')
  @UseInterceptors(FileInterceptor('file'))
  async importFile(@UploadedFile() file, @Res() res: Response): Promise<any> {
    const fullText: string = Buffer.from(file.buffer, 'binary').toString(
      'utf-8',
    );
    const allLines: string[] = fullText.split('\n');
    const elements: any[] = [];
    for (let index = 3; index < allLines.length; index++) {
      let element = allLines[index].split('\t').filter((e) => e.length > 0);
      elements.push({
        id: element[0],
        nome: element[1],
        marca: element[2],
        tipo: element[3],
        quantidade: element[4],
        preco: parseInt(element[5].replace(/^\D+/g, '')),
      });
    }
    return res.json(await this.itemService.saveImport(elements));
  }
}

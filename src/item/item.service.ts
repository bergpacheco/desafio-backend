import { Injectable } from '@nestjs/common';
import { Item } from '../database/models/item.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class ItemService {
  constructor(@InjectRepository(Item) private itemRepo: Repository<Item>) {}

  async novoItem(item: Item) {
    return await this.itemRepo.save(item);
  }

  async listItem() {
    return await this.itemRepo.find();
  }

  async deleteItem(id: number) {
    return await this.itemRepo.delete(id);
  }

  async saveImport(itens: Item[]) {
    return await this.itemRepo.save(itens);
  }
}

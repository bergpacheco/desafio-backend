import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Item {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  nome: string;

  @Column({ length: 500 })
  marca: string;

  @Column({ length: 200 })
  tipo: string;

  @Column('int')
  quantidade: number;

  @Column()
  preco: number;
}
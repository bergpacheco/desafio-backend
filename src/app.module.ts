import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemModule } from './item/item.module';
import { config as dotenv } from 'dotenv'
dotenv()
@Module({
  imports: [TypeOrmModule.forRoot(
    {
    name:"default",
    // @ts-ignore
    type: process.env.SQL_CONNECTION,
    host: process.env.SQL_HOST,
    port: parseInt(process.env.SQL_PORT),
    username: process.env.SQL_USERNAME,
    password: process.env.SQL_PASSWORD,
    database: process.env.SQL_DATABASE,
    entities: [__dirname + '/database/models/**/*.entity.{ts,js}'],
    logging: process.env.APP_ENV === 'DEBUG' ? true : false,
    options: { encrypt: false },
    synchronize: true,
  }
  ), ItemModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

FROM node:14-alpine

RUN apk add --no-cache tzdata

USER node

WORKDIR /home/node

COPY --chown=node:node *.json /home/node/

RUN yarn install

COPY --chown=node:node src /home/node/src

COPY --chown=node:node .env /home/node/

RUN yarn build

CMD [ "yarn" , "start:prod" ]
